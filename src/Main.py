# -*- coding: utf-8 -*-

#=======================
# =====Rickatomic======
#=======================

#main library
import time
import random
import pygame
from pygame.locals import *

#my library
import tick
import entity

COLOR_BACKGROUND, WHITE = ( random.randint( 50, 100 ), random.randint( 50, 100 ), random.randint( 50, 100 ),), ( 255, 255, 255 )
WIDTH, HEIGHT = 300, 200
pygame.init(); pygame.display.set_caption("pong")
screen = pygame.display.set_mode( ( WIDTH, HEIGHT ) )
screen_rect = screen.get_rect()
is_running = True

s_line = 6
w_line , h_line = ( WIDTH - s_line) // 2, HEIGHT

score = entity.Score( screen, screen_rect )
ball = entity.Ball( screen, screen_rect, score )
bat = entity.Bat( screen, screen_rect ,ball )
en_bat = entity.EnemyBat( screen, screen_rect ,ball , WIDTH - 10 )
tick = tick.Tick()

def logic():
	bat.logic( key )
	en_bat.logic( key )
	ball.logic( bat.bat_rect, en_bat.bat_rect )

def render():
	'''-*-BACKGROUND-*-'''
	screen.fill( COLOR_BACKGROUND )
	line = pygame.Surface( ( s_line, h_line ) )
	line.fill( WHITE )
	screen.blit(line, ( w_line, 0 ))
	'''-*-'''
	'''-*-SCORE-*-'''
	score.score_board()
	'''-*-'''
	'''-*-ENTITIES-*-'''
	bat.render()
	en_bat.render()
	ball.render()
	'''-*-'''

'''-*-DEBUG-*-'''
dbg_frm = 0
dbg_now = int( time.time() * 1000 )
'''-*-'''

while is_running:
	for event in pygame.event.get():
		if event.type == QUIT:
			is_running = False
			continue

	'''-*-KEYEVENT-*-'''
	key = pygame.key.get_pressed()
	'''-*-'''

	if tick.frame_looping( int( time.time() * 1000000 ) ):
		logic()
		render()
		'''-*-DEBUG-*-'''
		dbg_frm += 1
		'''-*-'''

	pygame.display.update()

	'''-*-DEBUG-*-'''
	if int( time.time() * 1000 ) - dbg_now >= 1000:
		print( "FPS: {}" .format( dbg_frm ) )
		dbg_now = int( time.time() * 1000 )
		dbg_frm = 0
	'''-*-'''

pygame.quit()