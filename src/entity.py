import pygame
from pygame.locals import *

class Score:
	def __init__(self, screen, screen_rect):
		pygame.font.init()
		self.font = pygame.font.Font(None, 32)
		self.score = [0, 0]
		self.screen = screen
		self.screen_rect = screen_rect 

	def score_board(self):
		self.text = self.font.render("{}      {}" .format( self.score[0], self.score[1] ), 1, ( 255, 255, 255 ))
		self.textpos = self.text.get_rect()
		self.textpos.centerx = self.screen.get_width() // 2
		self.screen.blit( self.text, self.textpos )
		self.screen.blit( self.screen, ( 0, 0 ) )

class Bat:
	def __init__(self, screen, screen_rect, ball, x = 0, y = 0, size = [10, 50]):
		self.bat = pygame.Surface( size )
		self.bat.fill( ( 255, 255, 255, ) )
		self.bat_rect = self.bat.get_rect()
		self.bat_rect[0] = x
		self.bat_rect[1] = y
		self.screen = screen
		self.screen_rect = screen_rect 
		self.ball = ball
		self.auto = False

	def logic(self, key, spd = 4):
		
		if ( key[pygame.K_d] ) and not self.auto:
			self.auto = True
		elif ( key[pygame.K_d] ):
			self.auto = False

		if self.auto:
			if self.bat_rect[1] + 25 > self.ball.ball_rect[1]:
				self.bat_rect[1] -= spd
			if self.bat_rect[1] + 25 < self.ball.ball_rect[1]:
				self.bat_rect[1] += spd
		else:
			if ( key[pygame.K_w] ):
			 self.bat_rect[1] -= spd
			elif ( key[pygame.K_s] ):
			 self.bat_rect[1] += spd

		self.bat_rect.clamp_ip( self.screen_rect )
		
	def render(self):
		self.screen.blit(self.bat, self.bat_rect)

class EnemyBat:
	def __init__(self, screen, screen_rect, ball, x = 0, y = 0, size = [10, 50]):
		self.bat = pygame.Surface( size )
		self.bat.fill( ( 255, 255, 255, ) )
		self.bat_rect = self.bat.get_rect()
		self.bat_rect[0] = x
		self.bat_rect[1] = y
		self.screen = screen
		self.screen_rect = screen_rect 
		self.ball = ball
		self.auto = False

	def logic(self, key, spd = 4):
		
		if ( key[pygame.K_RIGHT] ) and not self.auto:
			self.auto = True
		elif ( key[pygame.K_d] ):
			self.auto = False

		if self.auto:
			if self.bat_rect[1] + 25 > self.ball.ball_rect[1]:
				self.bat_rect[1] -= spd
			if self.bat_rect[1] + 25 < self.ball.ball_rect[1]:
				self.bat_rect[1] += spd
		else:
			if ( key[pygame.K_UP] ):
			 self.bat_rect[1] -= spd
			elif ( key[pygame.K_DOWN] ):
			 self.bat_rect[1] += spd

		self.bat_rect.clamp_ip( self.screen_rect )

	def render(self):
		self.screen.blit(self.bat, self.bat_rect)

class Ball:
	def __init__( self, screen, screen_rect, score, size = [7] * 2 ):
		self.w, self.h = size
		self.ball = pygame.Surface(size)
		self.ball.fill( ( 255, 255, 255, ) )
		self.screen = screen
		self.screen_rect = screen_rect 
		self.ball_rect = self.ball.get_rect()
		self.ball_rect.x = self.screen_rect.centerx
		self.ball_rect.y = self.screen_rect.centery
		self.dir = [1, 1]
		self.score = score

	def bump_bat( self, bat_rect ):
		if self.ball_rect.colliderect(bat_rect):
			self.dir[0] *= -1

	def bump_en_bat( self, en_bat_rect ):
		if self.ball_rect.colliderect(en_bat_rect):
			self.dir[0] *= -1

	def bump_wall( self ):
		if self.ball_rect.y <= 0 or self.ball_rect.y >= self.screen_rect.bottom - self.h:
			self.dir[1] *= -1
		if self.ball_rect.x <= 0 or self.ball_rect.x >= self.screen_rect.right - self.w:
			self.dir[0] *= -1
			if self.ball_rect.x <= 0:
				self.score.score[1] += 1
			if self.ball_rect.x >= self.screen_rect.right - self.w:
				self.score.score[0] += 1

	def logic( self, bat_rect, en_bat_rect, spd = 1 ):
		self.bump_bat( bat_rect )
		self.bump_en_bat( en_bat_rect )
		self.bump_wall()
		self.ball_rect[0] += self.dir[0] * spd
		self.ball_rect[1] += self.dir[1] * spd
		self.ball_rect.clamp_ip( self.screen_rect )

	def render(self):
		self.screen.blit(self.ball, self.ball_rect)
