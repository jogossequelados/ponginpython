import time

class Tick:
	def __init__(self):
		self.lst_time = int( time.time() * 1000000 )

	def frame_looping(self, crt_time, fps = 60.0):
		self.elapsed_time = ( crt_time - self.lst_time ) >= ( 1000000 / fps )

		if self.elapsed_time:
			self.lst_time = int( time.time() * 1000000 )

		return self.elapsed_time